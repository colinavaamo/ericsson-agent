var models = require('../models');
var express = require('express');
var router = express.Router();
var xmpp = require('simple-xmpp');


// Declaring conversation uuid and user uuid as global variables to be used with xmpp
var cid = '';
var uid = '';

const fetch = require('node-fetch');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];

var connection_options = [];

router.post('/', async function (req, res, next) {
  let b = req.body;

  let connections = {

  }

  console.log('Event: ' + b.event.event_type);
  if (b.event.event_type === 'new_chat_request') {
    cid = b.event.conversation.uuid;
    uid = b.event.user.layer_id;
    console.log('User: ' + b.event.conversation.display_name.split(',')[0]);
    console.log('Conversation UUID: ' + cid);
    console.log('User UUID: ' + uid)

    // Search for the conversation
    let queryResult = await models.User.findAndCountAll({
      where: {
        cid: cid
      }
    });

    // Add a user if not found
    if (queryResult.count == 0) {
      console.log('Adding new user');
      models.User.create({
        uid: uid,
        cid: cid
      });
    }
    // let connection = connections[]
    xmpp.connect({
      jid: `${queryResult.jid}@anon.localhost`,
      password: '',
      host: 'localhost',
      port: 5222
    });

    // xmpp.subscribe('colin@localhost');
    // xmpp.disconnect();
    console.log('Disconnecting...')
  } else if (b.event.event_type === 'chat_request_accepted') {

  } else if (b.event.event_type === 'user_message') {
    let cid = b.event.conversation.uuid;
    let queryResult = await models.User.findOne({
      where: {
        cid: cid
      }
    });

    let uid = queryResult.dataValues.uid;
    let jid = queryResult.dataValues.jid;
    let translate_from = await user_language(uid);

    let message = b.event.message.content;
    let translated = await translate(message, translate_from, 'en');

    console.log(b.event.conversation.display_name.split(',')[0] + ' says: ' + message);
    console.log('Translated: ' + translated.text);
    console.log('From: ' + translated.from);

    xmpp.connect({
      jid: `${jid}@anon.localhost`,
      password: '',
      host: 'localhost',
      port: 5222
    });

    console.log('Sending');
    let a = xmpp.send('colin@localhost', translated.text, false);
    console.log(a)

    xmpp.on('online', function (data) {
      console.log(`CID: ${cid} Connected with JID: ${data.jid.user}`);
      models.User.update({
        jid: data.jid.user
      }, {
        where: {
          cid: cid
        }
      })
    });

    xmpp.on('subscribe', function (from) {
      console.log("Subscription from :" + from);
    });

    xmpp.on('chat', async function (from, message, to) {
      console.log(from);
      console.log(to);
      console.log(message);

      let jid = to.split('@')[0];
      let queryResult = await models.User.findOne({
        where: {
          cid: cid
        }
      });
      let uid = queryResult.uid;
      let translated = await translate(message, 'en', await user_language(uid));

      sendMessage(translated.text, cid);
    });
  } else if (b.event.event_type == 'chat_terminated') {

    if (xmpp.conn) {
      xmpp.acceptUnsubscription('colin@localhost');
      xmpp.unsubscribe('colin@localhost');
      xmpp.disconnect();
      console.log('Disconnecting');
    } else {
      console.log('Not connected')
    }
  }

  console.log("API Ok");
  res.send().status(200);
});


/**
 * Translate text using microsoft translation services. 
 * Find the API reference here - https://docs.microsoft.com/en-us/azure/cognitive-services/translator/reference/v3-0-reference
 * 
 * The from parameter in fetch can be ignored so as to detect the incoming language.
 *
 * @param {message} - The text that needs to be translated
 * @param {from_language} - The language that the text needs to be translated from
 * @param {to_language} - The language that the text needs to be translated to
 * 
 * @returns {object} The return value contains the translated text and the detected language
 *
 * @example
 *     translate('hello', 'pt', 'en');
 */
let translate = (message, from_language, to_language) => {
  let body = [{
    "Text": message
  }];

  return fetch(`https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&from=${from_language}&to=${to_language}`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Ocp-Apim-Subscription-Key': config.microsoft_token,
      'Content-Type': 'application/json'
    }
  }).then((res) => {
    return res.json();
  }).then((json) => {
    console.log(json);
    return {
      text: json[0].translations[0].text,
      from: from_language
    }
  })
}

/**
 * Get the user's language set in the workflow using the storage API. 
 *
 * @param {uid} - layer_id for the user. Stored in database, initially fetched from webhook request for 'new_chat_request'
 * @param {key} - Storage key to fetch the value. Defaults to 'language'
 * 
 * @returns {string} The return value contains the stored value for given key in the bot
 *
 * @example
 *     user_language('dashboard_admin_test_user_1');
 */
let user_language = (uid, key = 'language') => {
  return fetch(`https://${config.server}.avaamo.com/bots_api/v1/users/${uid}/get_storage.json?bot_id=${config.bot_id}&key=${key}`, {
    method: 'GET',
    headers: {
      'Access-Token': config.dashboard_token,
      'Content-Type': 'application/json'
    }
  }).then((res) => {
    return res.json();
  }).then((json) => {
    console.log(json)
    return json.value;
  })
};

/**
 * Send a message to the user as the live agent
 *
 * @param {message} - The message to be send
 * @param {cid} - The conversation id for the user's conversation
 * 
 * @returns {string} The return value contains the stored value for given key in the bot
 *
 * @example
 *     user_language('dashboard_admin_test_user_1');
 */
let sendMessage = (message, cid) => {
  let body = {
    "message": {
      "conversation": {
        "uuid": cid
      },
      "content": message,
      "content_type": "text"
    }
  }

  return fetch(`https://c5.avaamo.com/live_agent/custom/messages.json?access_token=${config.liveagent_token}`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((res) => {
    return res.json();
  }).then((json) => {
    console.log("RES: " + json)
  })
}

module.exports = router;